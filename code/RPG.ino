// =====| Tune these variables |======
//                                       Default|
int minIntensityDiff = 45;              //60    | For each of the 7 bands
int minIntensityBass = 55;              //55    | Intensity for only bass (63Hz and 163Hz bands) NOT YET IMPLEMENTED
int minBeatInterval = 150;              //150   | Beats that occur in intervals shorter than this are omitted (milliseconds)

int silenceThreshold = 20;              //20    | Average intensity value below which silence detection starts
int silenceAfterMSec = 2000;            //2000  | Silence detected after this many milliseconds.

// Rhythmic Pattern Generator
int rpgMinTimeSinceLastTrigger = 330;   //330   | Adjusts the sensitivity of the first rpg output (start of a bar). Higher values mean that the output goes HIGH less often.
int counterListMaxValue = 500;
int recoveryThreshold = 3000;           //3000  | Recovers previously detected beats if playback continues after this many milliseconds
const int beatListIntervalErrorMargin = 20;
const int beatListErrorMargin = 10;

// =====| Tune those variables |=====

int avgIntensity = 0;
int prevAvgIntensity = 0;


class Band {
  public:
    int pin;
    int id;             //for biased sensitivity (WIP)
    int intensity;
    int lastIntensity;
    int intensityDiff;
    String trigger;     //for debugging purposes
    byte state;
    
    Band(int p, int i){
      pin = p;
      id = i;
      intensity = 0;
      lastIntensity = 0;
      intensityDiff = 0;
      trigger = " ";
      state = LOW;
      pinMode(pin, OUTPUT);     
    }

    void add(int value){
      lastIntensity = intensity;
      intensity = value;
      intensityDiff = intensity - lastIntensity;

      if (intensityDiff > minIntensityDiff){
        digitalWrite(pin, HIGH);
        state = HIGH;
        trigger = "I";
      } else {
        digitalWrite(pin, LOW);
        state = LOW;
        trigger = " ";
      }
    }

    byte getState(){
      return state;
    }

    int getPin(){
      return pin;
    }

    int getIntensity(){
      return intensity;
    }

    int getLastIntensity(){
      return lastIntensity;
    }

    int getIntensityDiff(){
      return intensityDiff;
    }

    String getTrigger(){
      return trigger;
    }
    
};

class Counter {
  public:
    int interval;
    int count;
    unsigned long timeSinceLastIncrement;
    int rateFactor; //WIP
    bool empty;
    Counter(int i){
      interval = i;
      count = 0;
      empty = true;
      updateTime();
    }

    void updateTime(){
      timeSinceLastIncrement = millis();
    }

    int getTimeSinceLastIncrement(){
      return millis() - timeSinceLastIncrement;
    }

    int getInterval(){
      return interval;
    }

    int getCount(){
      return count;
    }

    void setInterval(int value){
      interval = value;
      empty = false;
      updateTime();
    }

    void setCount(int value){
      count = value;
      empty = false;
      updateTime();
    }


    void addCount(){
      count++;
      empty = false;
      updateTime();
    }

    void reset(){
      interval = 0;
      count = 0;
      empty = true;
      updateTime();
    }

    bool isEmpty(){
      return empty;
    }
};

// MSGEQ7 Pins
const int resetPin = 3;
const int strobePin = 2;
const int outputPin = A0;

// Array
Band bands[7] = {Band(7, 0), Band(8, 1), Band(9, 2), Band(10, 3), Band(11, 4), Band(12, 5), Band(13, 6)};


//Beat detection variables
unsigned long currentTime = 0;
unsigned long prevTriggerTime = 0;
unsigned long startOfSilence = 0;

int timeSinceLastTrigger = 0;
int countTrigger = 0;

const int beat1Pin = 4;
const int beat2Pin = 5;
const int tempoPin = 6;



const int counterListMaxElements = 10;
bool counterListHasBeenReset = false;
bool counterListRecoveryActive = false;

unsigned long timeSinceReset;
unsigned long timeSincePlayback;
unsigned long timeSinceTempoLedWasHigh;
Counter counterList[counterListMaxElements] = {Counter(0),Counter(0),Counter(0),Counter(0),Counter(0),Counter(0),Counter(0),Counter(0),Counter(0),Counter(0)};
//Counter backup[counterListMaxElements] = counterList[counterListMaxElements];

//Counter backup[counterListMaxElements];

byte tempoLedState = LOW;


void setup() {
  Serial.begin(9600);

  //MSGEQ7 control pins
  pinMode(resetPin, OUTPUT);
  pinMode(strobePin, OUTPUT);

  digitalWrite(resetPin, LOW);
  digitalWrite(resetPin, HIGH);
  //backup[counterListMaxElements] = counterList[counterListMaxElements];

}

//=====| Main Loop |=====
void loop() { //Update before printing
  readMSGEQ7();
  
  //printValues();
  updateTriggers();
  
  //printTriggers(); 
  
  updateTimeSinceLastTrigger();
  printTimeSinceLastTrigger();

  outputRPG();
  outputTempo();

  printCounterList();
  
  
  currentTime = millis();
  Serial.println("");  
}



bool checkIfPlaying(){
  if (avgIntensity > silenceThreshold && prevAvgIntensity > silenceThreshold) startOfSilence = millis();
  int silence = currentTime - startOfSilence;
  //if (silence > 100) Serial.print(silence);
  if (silence < silenceAfterMSec){
    counterListHasBeenReset = false;
    timeSincePlayback = currentTime;
    return true;
  }
  else {
    return false;
  }
}

void outputRPG(){
  //Rhythmic pattern generator
  if (countTrigger >= 2 && timeSinceLastTrigger > rpgMinTimeSinceLastTrigger) digitalWrite(beat1Pin, HIGH);

  else if (countTrigger >= 2) digitalWrite(beat2Pin, HIGH);
  else {
    digitalWrite(beat1Pin, LOW);
    digitalWrite(beat2Pin, LOW);       
  }
}

void outputTempo(){
  if (checkIfPlaying()){
    if (counterListRecoveryActive){
      Serial.print("recovering");
      counterListRecoveryActive = false;
    }
    
    counterListAdd(timeSinceLastTrigger);
    //if (beatListGetHighest() = timeSinceLastTrigger) sync();
    counterListRemoveUnused();
    //counterListCombineSimilar();
    //counterListCombineMultiple();
    counterListOrganize();
    //counterListBackup(&counterList, &backup, counterListMaxElements);
    
    if (counterList[0].getCount() > 6){
      if (currentTime - timeSinceTempoLedWasHigh > counterList[0].getInterval()){
        timeSinceTempoLedWasHigh = currentTime;
        toggleLed(tempoPin);
      }
    }
  } else {
    counterListReset();
    counterListRecoveryActive = true;
    resetLed(tempoPin);
    
    if (currentTime - timeSincePlayback > recoveryThreshold){
      counterListRecoveryActive = false;

    }
  }
}

void toggleLed(int pin){
  if (tempoLedState == LOW){
    digitalWrite(pin, HIGH);
    tempoLedState = HIGH;
  } else {
    digitalWrite(pin, LOW);
    tempoLedState = LOW;
  }
}

void resetLed(int pin){
  digitalWrite(pin, LOW);
}

//=====| BeatList functions |=====


void counterListOrganize(){
  int i = 1;
  while (i < counterListMaxElements){
    int j = i;
    while (j > 0 && counterList[j - 1].getCount() < counterList[j].getCount()){
      swap(&counterList[i], &counterList[j - 1]);
      j = j - 1;
    }
    i = i + 1;
  }
}

void swap(Counter* a, Counter* b){
  Counter temp = *a;
  *a = *b;
  *b = temp;
}


void counterListAdd(int value){  
  if (value == 0 || value > counterListMaxValue) return;
  if (countTrigger >= 2 && timeSinceLastTrigger > minBeatInterval) {
    if (counterListContainsSimilarValue(value)){
      
    } else {
      (counterListHasSpace(value));
    }
  }
}

void counterListRemoveUnused(){
  for (int i = 0; i < counterListMaxElements; i++){
    if (counterList[i].getCount() <= 3 && counterList[i].getTimeSinceLastIncrement() > 5000){
      counterList[i].reset();
    } else if (counterList[i].getCount() <= 6 && counterList[i].getTimeSinceLastIncrement() > 10000){
      counterList[i].reset();
    } else if (counterList[i].getCount() <= 9 && counterList[i].getTimeSinceLastIncrement() > 100000){
      counterList[i].reset();
    }
  }
}

void counterListReset(){
  if (counterListHasBeenReset) return;
  else {
    Serial.print("resetting");
    counterListHasBeenReset = true;
    timeSinceReset = currentTime;

    for (int i = 0; i < counterListMaxElements; i++){
      counterList[i].reset();
    }
  }
}

bool counterListIsEmpty(Counter arr[]){
  for (int i = counterListMaxElements - 1; i >= 0; i--){
    int interval = arr[i].getInterval();
    int count = arr[i].getCount();
    if (interval != 0){
      return false;
    }
    if (count != 0){
      return false;
    }
  }
  return true;
}

bool counterListContainsSimilarValue(int value){
  for (int i = 0; i < counterListMaxElements; i++){
    if (counterList[i].isEmpty()){
      continue;
    } else if (value >= counterList[i].getInterval() - beatListErrorMargin && value <= counterList[i].getInterval() + beatListErrorMargin){
      int valueOld = counterList[i].getInterval();
      counterList[i].setInterval((value + valueOld) / 2);
      counterList[i].addCount();
      return true;
    }
  }
  return false;
}

bool counterListHasSpace(int value){
  for (int i = 0; i < counterListMaxElements; i++){
    if (counterList[i].getInterval() == 0 || counterList[i].isEmpty()){
      counterList[i].setInterval(value);
      counterList[i].addCount();
      return true;
    }
    
  }
  return false;
}

void counterListBackup(Counter** source[], Counter** dest[], int l){
  *dest[l] = *source[l];
}
/*
void beatListCombineToCommonFactor(int value){
  for (int i = 0; i < beatListMaxElements; i++){
    int interval = beatList[i][0];
    if (interval != 0){
      if (value > interval){ //is a multiple of
        for (int j = 2; j <= 4; j++){
          if (value > (j * interval - beatListIntervalErrorMargin) && value < (j * interval + beatListIntervalErrorMargin)){
            beatListAddTo(interval * j);
            return;
          }
        }
      }
      if (value < interval){ //is divisible by
        for (int j = 2; j <= 4; j++){
          if (value > (j / interval - beatListIntervalErrorMargin) && value < (j / interval + beatListIntervalErrorMargin)){
            beatListAddTo(interval / j);
            return;
          }
        }        
      }
    }
  }
}
*/
void counterListAddTo(int value){
  for (int i = 0; i < counterListMaxElements; i++){
    if (counterList[i].getInterval() == value){
      counterList[i].addCount();
      return;
    }
    else i++;
  }
}





void updateTimeSinceLastTrigger(){
  timeSinceLastTrigger = currentTime - prevTriggerTime;
  if (countTrigger >= 2 && timeSinceLastTrigger > minBeatInterval){
    prevTriggerTime = currentTime;
  } //else if (!beatDetected())
}

void updateTriggers(){
  countTrigger = 0;

  for (int i = 0; i < 7; i++) {
    String data = bands[i].getTrigger();
    if (bands[i].getState() == HIGH){
      countTrigger++;
    }    
  }
}

//=====| Print functions |=====
void printCounterList(){
  for (int i = 0; i < counterListMaxElements; i++){
    Serial.print("{");
    Serial.print(counterList[i].getInterval());
    Serial.print(",");
    Serial.print(counterList[i].getCount());
    //Serial.print(",");
    //Serial.print(counterList[i].getTimeSinceLastIncrement(currentTime));
    Serial.print("}");
  }
}
void printValues(){
  for (int i = 0; i < 7; i++) {
    int intensity = bands[i].getIntensity();
    if (intensity < 100) Serial.print(" ");
    if (intensity < 10) Serial.print(" ");
    Serial.print(intensity);
    Serial.print(" ");
  }
  Serial.print(" | ");
  if (avgIntensity < 100) Serial.print(" ");
  if (avgIntensity < 10) Serial.print(" ");
  Serial.print(avgIntensity); 
}

void printTriggers(){
// X = triggered from exceeding threshold value 
// I = triggered from a sudden change in intensity

  for (int i = 0; i < 7; i++) {
    String data = bands[i].getTrigger();
    Serial.print(" | " + data);
  }
  Serial.print(" || ");
  
  if (countTrigger > 1) Serial.print(countTrigger);
  else Serial.print(" ");
  Serial.print(" || ");
}

void printTimeSinceLastTrigger(){
  if (countTrigger >= 2 && timeSinceLastTrigger > minBeatInterval){
    if(timeSinceLastTrigger < 100000) Serial.print(" ");
    if(timeSinceLastTrigger < 10000) Serial.print(" ");
    if(timeSinceLastTrigger < 1000) Serial.print(" ");
    if(timeSinceLastTrigger < 100) Serial.print(" ");
    if(timeSinceLastTrigger < 10) Serial.print(" ");
    
    Serial.print(timeSinceLastTrigger);
    Serial.print(" | ");
  } else {
    Serial.print("       | ");
  }
}

void readMSGEQ7() {
  digitalWrite(resetPin, HIGH);
  digitalWrite(resetPin, LOW);
  delayMicroseconds(40);
  int data = 0;

  int sum = 0;
  for (int i = 0; i < 7; i++) {
    digitalWrite(strobePin, LOW);
    delayMicroseconds(40);
    int data = analogRead(outputPin) / 4;

    bands[i].add(data);
    sum += data;

    digitalWrite(strobePin, HIGH);
    delayMicroseconds(40);
  }
  prevAvgIntensity = avgIntensity;
  avgIntensity = sum / 7.0;
  
}
